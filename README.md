###GETTING STARTED WITH GIT
https://confluence.int.zone/display/APS/GIT+Getting+started+for+new+developers
 
##HOW TO START DEVELOPING
###Install nodejs
* ```curl https://raw.githubusercontent.com/creationix/nvm/v0.7.0/install.sh | sh```
* Restart your terminal or run ```source ~/.bash_profile``` 
* Install last node version ```nvm install --lts```
* Set default node version ```nvm alias default `node --version` ```

###Get sources
* Clone project ```git clone https://maxshilov@bitbucket.org/maxshilov/piper-ui.git```
* ```cd piper-ui```
* Configure backend URL in ```config.js```
* Install dependencies ```npm install```

###How to tun tests and dev server
* Run tests ```npm run test```
* Run dev server ```npm run server:dev``` or ```npm run runserver```(without tslint)