#!/usr/bin/env bash
set -e
source ~/.bash_profile
npm install
npm --no-git-tag-version version 0.1.${BUILD_NUMBER}
npm run test
if [ "$*" = "--prod" ]
then
  sed -i 's/__SCOPE__/prod/g' package.json
  npm run build
  sonar-scanner -Dsonar.projectVersion=0.1.${BUILD_NUMBER}
else
  sed -i 's/__SCOPE__/dev/g' package.json
  npm run build:dev
fi

npm publish



