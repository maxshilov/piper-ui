declare const ConfigData: any;

class AppConfig {
  private backendUrl = 'http://' + ConfigData.backendHostPort  + '/';
  private apiPath = '/api/v1/';
  private wsServiceUrl = 'ws://' + ConfigData.backendHostPort + '/';
  private countries = [
    {name: 'United States', code: 'us'},
    {name: 'Master Dev', code: 'dev'},
    {name: 'Europe', code: 'eu'},
    {name: 'APAC', code: 'apac'}
  ];
  private storage = window.sessionStorage;
  // Azure auth params
  private tenant = ConfigData.tenant;
  private scope = 'openid offline_access User.Read';
  private redirectUri = window.location.protocol + '//' + window.location.host + '/login';
  private appId = ConfigData.appId;
  private nonce = Math.floor(Math.random() * 1000);
  private authorizeUrl = `https://login.microsoftonline.com/${this.tenant}/oauth2/v2.0/authorize \
                          ?client_id=${this.appId} \
                          &scope=${encodeURIComponent(this.scope)} \
                          &redirect_uri=${encodeURIComponent(this.redirectUri)} \
                          &response_type=id_token+token \
                          &nonce=${this.nonce}`;

  private logoutUrl = `https://login.microsoftonline.com/${this.tenant}/oauth2/logout`;

  public getBackendApiUrl(subPath: string): string {
    return this.backendUrl + this.apiPath + subPath;
  }

  public getWSUrl(subPath: string){
    return this.wsServiceUrl + subPath;
  }

  public getBackendUrl() {
    return this.backendUrl;
  }

  public countriesList(){
    return this.countries
  }

  public getAuthorizeUrl() {
    return this.authorizeUrl;
  }

  public getLogoutUrl() {
    return this.logoutUrl;
  }

  public getStorage() {
    return this.storage;
  }

  public getSession() {
    return this.getStorage().getItem('apisessionid');
  }

  public getSessionHash() {
    return this.getStorage().getItem('sessionhash');
  }
}

export let Config: AppConfig = new AppConfig();
