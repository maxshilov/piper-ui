export class User {
  public id: number;
  public name: string;
  public 'first_name': string;
  public 'last_name': string;
  public email?: string;
  public username?: string;
  public 'date_joined': Date;
}
