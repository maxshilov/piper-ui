import {Country} from "./country";


export class ResellerSyncParams {
  public country: Country;
  public resellerId: number = 0;
  public serviceTemplateId: number = 0;
  public updateManaged: boolean = false;
  public dryRun: boolean = false;
  public syncPlan: boolean = false;
  public syncPlanChange: boolean = false;
  public syncPlanIcons: boolean = false;
  public syncSalesCat: boolean = false;
  public syncSalesCatIcons: boolean = false;
  public syncPlanCat: boolean = false;
  public syncResCat: boolean = false;
}
