import { FileUploader, FileUploaderOptions, FilterFunction } from 'ng2-file-upload';
import { Subject, Observable } from 'rxjs';
import { ErrorMessageHolder } from '../../common/error-message-holder';

export class Uploader extends FileUploader {
  public filename: Observable<string>;
  private filenameSource: Subject<string> = new Subject<string>();

  constructor(options: FileUploaderOptions, private errorMessageHolder: ErrorMessageHolder) {
    super(options);
    this.filename = this.filenameSource.asObservable();
  }

  public addToQueue(files: File[], options?: FileUploaderOptions, filters?: FilterFunction[] | string): void {
    this.clearQueue();
    super.addToQueue(files, options, filters);
    let items = this.getNotUploadedItems();
    if (items.length) {
      let item = items[items.length - 1];
      this.filenameSource.next(item.file.name);
      this.errorMessageHolder.setErrorMessage(null);
    }
  }
}
