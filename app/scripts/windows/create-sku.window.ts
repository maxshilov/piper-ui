import { Component } from '@angular/core';
import { DataProvider } from '../../common/data-provider';
import { Router } from '@angular/router';
import { CommunicationService } from '../../app-communication.service';
import { PollingService } from '../../common/polling.service';
import { MenuItems } from '../../common/static-menu-items';
import { CommonWindow } from '../../layout/common.window';

@Component({
  templateUrl: 'create-sku.template.html'
})
export class CreateSKUWindow extends CommonWindow {
  public resellerId:any;
  public dryRun:any;
  public checkPrices:any;
  public skipBundles:any;


  constructor(protected communicationService: CommunicationService,
              protected router: Router,
              protected dataProvider: DataProvider,
              protected pollingService: PollingService) {
    super(communicationService);
    this.communicationService.setBreadcrumbs([MenuItems.DASHBOARD, MenuItems.CREATE_SKU]);
  }

  public getErrorMessage():string {
    return '';
  }
  public createSku():any {
    return 'hello';
  }
}
