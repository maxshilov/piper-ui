import {Component} from "@angular/core";
import {DataProvider} from "../../common/data-provider";
import {Router} from "@angular/router";
import {CommunicationService} from "../../app-communication.service";
import {PollingService} from "../../common/polling.service";
import {CommonWindow} from "../../layout/common.window";

@Component({
  templateUrl: 'dashboard.template.html'
})
export class DashboardWindow extends CommonWindow {

  constructor(protected communicationService: CommunicationService,
              protected router: Router,
              protected dataProvider: DataProvider,
              protected pollingService: PollingService) {
    super(communicationService);
  }
}
