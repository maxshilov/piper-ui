import {Component} from "@angular/core";
import {DataProvider} from "../../common/data-provider";
import {Router} from "@angular/router";
import {CommunicationService} from "../../app-communication.service";
import {MenuItems} from "../../common/static-menu-items";
import {CommonWindow} from "../../layout/common.window";
import {ResellerSyncParams} from "../../model/reseller-sync.params";
import {WebSocketService} from "../../common/websocket.service";
import {Config} from "../../app-config";
import {Subject} from "rxjs";

@Component({
  selector: 'reseller-sync',
  templateUrl: 'reseller-sync.template.html'
})
export class ResellerSyncWindow extends CommonWindow {
  public params: ResellerSyncParams;
  public logs: Subject<any>;
  public logArray: string[];


  constructor(protected communicationService: CommunicationService,
              protected router: Router,
              protected dataProvider: DataProvider,
              protected websocketService: WebSocketService) {
    super(communicationService);
    this.communicationService.setBreadcrumbs([MenuItems.DASHBOARD, MenuItems.SYNC_RESELLER]);
    this.params = new ResellerSyncParams();
    this.logArray = [];
  }

  public static getErrorMessage():string {
    return '';
  }
  public syncReseller():any {
    this.logArray = [];
    // add selected country to script parameters
    this.params.country = this.communicationService.getCurrentCountry();
    return this.dataProvider.syncReseller(this.params).subscribe((task_resp:any) => {
      console.log("Task name");
      console.log(task_resp);
      this.logs = this.websocketService.connect( Config.getWSUrl(task_resp.group) );
      this.logs.subscribe(
        (resp) => {
          console.log("MSG(logs) from BE");
          console.log(resp.data);
          this.logArray.push(resp.data);
        },
        (err) => {console.log(err);}
      );
    });
  }

}
