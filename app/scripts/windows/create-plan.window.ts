import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Config } from '../../app-config';
import { CommunicationService } from '../../app-communication.service';
import { CommonWindow } from '../../layout/common.window';
import { MenuItems } from '../../common/static-menu-items';
import { Uploader } from '../components/uploader';
import { ErrorMessageHolder } from '../../common/error-message-holder';
import {WebSocketService} from "../../common/websocket.service";
import {Subject} from "rxjs";

@Component({
  selector: 'create-plan',
  templateUrl: 'create-plan.template.html'
})
export class CreatePlanWindow extends CommonWindow implements OnInit, ErrorMessageHolder {
  public uploader: Uploader = new Uploader({
    url: Config.getBackendApiUrl('plan'),
    headers: [{name: 'x-csrftoken', value: this.communicationService.getCSRFToken()}],
  }, this);

  public vendorId: string;
  public checkExisting: boolean;
  private errorMessage: string;
  private name: string;

  public logs: Subject<any>;
  public logArray: string[];

  constructor(protected communicationService: CommunicationService,
              private router: Router, private websocketService: WebSocketService)
  {
    super(communicationService);
    this.logArray = [];
  }

  public ngOnInit() {
    this.communicationService.activateMenuItem(MenuItems.DASHBOARD);
    this.communicationService.setBreadcrumbs([MenuItems.DASHBOARD, MenuItems.CREATE_PLAN]);

    this.uploader.filename.subscribe((filename) => this.name = filename.substring(0, filename.lastIndexOf('.')));
    this.uploader.onSuccessItem = (item, response, status, header) => {
      // this.router.navigate(['/']);
    };
    this.uploader.onErrorItem = (item, response, status, header) => {
      try {
        let error = JSON.parse(response);
        this.setErrorMessage(error.detail);
      } catch (Exception) {
        this.setErrorMessage('Unexpected error');
      } finally {
        item.isUploaded = false;
      }
    };
    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      form.append('name', this.name);
      form.append('vendorId', this.vendorId);
      form.append('checkExisting', this.checkExisting);
      form.append('filename', fileItem.file.name);
    };
  }

  public setErrorMessage(message: string): void {
    this.errorMessage = message;
  }

  public getErrorMessage(): string {
    return this.errorMessage;
  }

  public upload() {
    let items = this.uploader.getNotUploadedItems();
    if (items.length) {
      let item = items[items.length - 1];
      item.upload();
    }

    this.logs = this.websocketService.connect( Config.getWSUrl('plan') );
    this.logs.subscribe(
      (resp) => {
        console.log("MSG(logs) from BE");
        console.log(resp.data);
        this.logArray.push(resp.data);
      },
      (err) => {
        console.log(err);
      }
    );

  }
}
