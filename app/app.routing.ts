import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './login/auth-guard';
import { DashboardWindow } from './scripts/windows/dashboard.window';
import { LoginWindow } from './login/login.window';
import { CreateSKUWindow } from './scripts/windows/create-sku.window';
import { ResellerSyncWindow } from './scripts/windows/reseller-sync.window';
import {CreatePlanWindow} from "./scripts/windows/create-plan.window";

const appRoutes: Routes = [
  {path: '', component: DashboardWindow, canActivate: [AuthGuard]},
  {path: 'dashboard', component: DashboardWindow, pathMatch: 'full', canActivate: [AuthGuard, ]},
  {path: 'pba/create-sku', component: CreateSKUWindow, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'pba/sync-reseller', component: ResellerSyncWindow, pathMatch: 'full', canActivate: [AuthGuard, ]},
  {path: 'pba/create-plan', component: CreatePlanWindow, pathMatch: 'full', canActivate: [AuthGuard, ]},
  {path: 'login', component: LoginWindow},
  // otherwise redirect to home
  {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes);
