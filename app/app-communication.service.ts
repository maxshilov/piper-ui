import { Injectable } from '@angular/core';
import { Country } from './model/country';
import { User } from './model/user';
import { CommonActionItem } from './common/common-action-item';
import { MenuItems } from './common/static-menu-items';
import { Observable, Subject } from 'rxjs';
import { Config } from './app-config';

@Injectable()
export class CommunicationService {
  public currentCountry: Observable<Country>;
  public activeMenuItem: Observable<CommonActionItem>;
  public breadcrumbs: Observable<CommonActionItem[]>;
  public actions: Observable<CommonActionItem[]>;
  public isLoginRequired: Observable<boolean>;
  private currentCountrySource = new Subject<Country>();
  private activeMenuItemSource = new Subject<CommonActionItem>();
  private breadcrumbsSource = new Subject<CommonActionItem[]>();
  private actionsSource = new Subject<CommonActionItem[]>();
  private isLoginRequiredSource = new Subject<boolean>();
  private storage: Storage = Config.getStorage();

  constructor() {
    this.currentCountry = this.currentCountrySource.asObservable();
    this.activeMenuItem = this.activeMenuItemSource.asObservable();
    this.breadcrumbs = this.breadcrumbsSource.asObservable();
    this.actions = this.actionsSource.asObservable();
    this.isLoginRequired = this.isLoginRequiredSource.asObservable();

    this.currentCountrySource.next(this.getCurrentCountry());
  }

  public isAuthenticated(): boolean {
    return !!this.session && !!this.user;
  }

  public getCurrentCountry() {
    return this.getLocal('currentCountry');
  }

  public getCurrentCountryCode() {
    let country = this.getLocal('currentCountry');
    if (country && country.code) {
      return country.code;
    } else {
      return;
    }
  }

  public selectCountry(country: Country): void {
    this.changeCountry(country);
  }

  public changeCountry(country: Country) {
    this.setOrRemoveLocal(country, 'currentCountry');
    this.currentCountrySource.next(country);
    return this.currentCountry;
  }

  set user(user: User) {
    this.setOrRemoveLocal(user, 'user');
  }

  get user() {
    return this.getLocal('user');
  }

  public getUserName(): string {
    return this.user.name;
  }

  get idToken(): string {
    return this.getLocal('id_token');
  }

  set idToken(idToken: string) {
    this.setOrRemoveLocal(idToken, 'id_token');
  }

  get session(): string {
    return this.storage.getItem('apisessionid');
  }

  set session(sessionId: string) {
    this.storage.setItem('apisessionid', sessionId);
  }

  get sessionHash(): string {
    return this.storage.getItem('sessionhash');
  }

  set sessionHash(sessionHash: string) {
    this.storage.setItem('sessionhash', sessionHash);
  }

  public setCSRFToken(token: string) {
    if (token) {
      this.storage.setItem('csrf_token', token);
    }
  }

  public getCSRFToken(): string {
    return this.storage.getItem('csrf_token');
  }

  public activateMenuItem(item: CommonActionItem): void {
    this.activeMenuItemSource.next(item);
  }

  public setBreadcrumbs(items: CommonActionItem[]): void {
    this.breadcrumbsSource.next(items);
  }

  public setActions(items: CommonActionItem[]): void {
    this.actionsSource.next(items);
  }

  public setLoginRequired() {
    this.isLoginRequiredSource.next(true);
  }

  public clearScreen(): void {
    this.activateMenuItem(MenuItems.DASHBOARD);
    this.setBreadcrumbs([]);
    this.setActions([]);
  }

  private getLocal(key: string): any {
    return JSON.parse(this.storage.getItem(key));
  }

  private setOrRemoveLocal(obj: any, key: string): void {
    if (obj) {
      this.storage.setItem(key, JSON.stringify(obj));
    } else {
      this.storage.removeItem(key);
    }
  }
}
