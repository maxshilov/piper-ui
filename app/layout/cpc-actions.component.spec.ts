import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
// Load the implementations that should be tested
import { CommunicationService } from '../app-communication.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { CpcActionsComponent } from './cpc-actions.component';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe(`Actions Component`, () => {
  let comp: CpcActionsComponent;
  let fixture: ComponentFixture<CpcActionsComponent>;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([])],
      declarations: [CpcActionsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        CookieService,
        CommunicationService,
        {provide: APP_BASE_HREF, useValue: '/'}]
    })
        .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(CpcActionsComponent);
    comp = fixture.componentInstance;

    fixture.detectChanges(); // trigger initial data binding
  });

  it(`should be readly initialized`, () => {
    expect(fixture).toBeDefined();
    expect(comp).toBeDefined();
  });
});
