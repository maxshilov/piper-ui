import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'dialog-confirm',
  templateUrl: 'cpc-confirmation.template.html'
})
export class ConfirmDialog {
  public dialogTitle: string;
  public dialogMessage: string;

  constructor(public dialogRef: MdDialogRef<ConfirmDialog>) {
  }
}
