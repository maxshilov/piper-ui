import {Component} from "@angular/core";
import {AuthenticationService} from "../login/auth.service";
import {DataProvider} from "../common/data-provider";
import {Router} from "@angular/router";
import {CommunicationService} from "../app-communication.service";
import {Country} from "../model/country";
import {Config} from "../app-config";

@Component({
  selector: 'cpc-header',
  templateUrl: 'cpc-header.template.html'
})
export class CpcHeaderComponent {
  public currentCountry: Country;
  private countries: Country[] = [];

  constructor(private router: Router,
              private authService: AuthenticationService,
              private communicationService: CommunicationService,
              private dataProvider: DataProvider)
  {
    this.countries = Config.countriesList();
    this.currentCountry = this.communicationService.getCurrentCountry();
  }

  public getUserName(): string {
    return this.communicationService.getUserName();
  }

  public logout() {
    this.authService.logout();
  }

  selectCountry(country: Country): void {
    this.communicationService.selectCountry(country);
    this.currentCountry = country;
  }
}
