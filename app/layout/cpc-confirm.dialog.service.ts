import { Injectable } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ConfirmDialog } from './cpc-confirm.dialog';

@Injectable()
export class DialogConfirmationService {

  constructor(public dialog: MdDialog) {
  }

  public openContentConfirmation(title: string, message: string, callback: () => void) {
    let dialogRef: MdDialogRef<ConfirmDialog> = this.dialog.open(ConfirmDialog, {disableClose: true});
    dialogRef.componentInstance.dialogTitle = title;
    dialogRef.componentInstance.dialogMessage = message;
    dialogRef.afterClosed().subscribe((result) => result && callback());
  }
}
