import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommunicationService } from '../app-communication.service';
import { CommonActionItem } from '../common/common-action-item';

@Component({
  selector: 'cpc-breadcrumb',
  templateUrl: 'cpc-breadcrumb.template.html'
})
export class CpcBreadcrumbComponent implements OnInit {
  private breadcrumbs: CommonActionItem[];

  constructor(private router: Router,
              private communicationService: CommunicationService) {
  }

  public ngOnInit(): void {
    this.communicationService.breadcrumbs.subscribe((breadcrumbs) => this.breadcrumbs = breadcrumbs);
  }
}
