import { CommunicationService } from '../app-communication.service';

// Please, extend this class for implementing new window. You can find template below:
/*

 import {CommonWindow} from "../layout/common.window";

 @Component({
   moduleId: module.id,
   templateUrl: 'new-window.template.html'
 })
 export class NewWindow extends CommonWindow implements OnInit {

   constructor(protected communicationService: CommunicationService) {
     super(communicationService);
   }
 }
 */

export abstract class CommonWindow {
  constructor(protected communicationService: CommunicationService) {
    this.communicationService.clearScreen();
  }
}
