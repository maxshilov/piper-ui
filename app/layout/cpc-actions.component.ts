import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommunicationService } from '../app-communication.service';
import { CommonActionItem } from '../common/common-action-item';

@Component({
  selector: 'cpc-actions',
  templateUrl: 'cpc-actions.template.html'
})
export class CpcActionsComponent implements OnInit {
  private actions: CommonActionItem[];

  constructor(private router: Router,
              private communicationService: CommunicationService) {
  }

  public ngOnInit(): void {
    this.communicationService.actions.subscribe((actions) => this.actions = actions);
  }

  public actionClick(action: CommonActionItem): void {
    if (!action.disabled) {
      if (action.command) {
        action.command();
      } else if (action.routerLink) {
        this.router.navigate([action.routerLink]);
      }
    }
  }
}
