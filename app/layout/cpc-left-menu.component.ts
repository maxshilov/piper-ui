import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CommunicationService } from '../app-communication.service';
import { CommonActionItem } from '../common/common-action-item';
import { MenuItems } from '../common/static-menu-items';

@Component({
  selector: 'cpc-left-menu',
  templateUrl: 'cpc-left-menu.template.html'
})
export class CpcLeftMenuComponent {
  public activeMenuItem: CommonActionItem = null;
  private menuItems: CommonActionItem[] = [
    MenuItems.DASHBOARD,
    MenuItems.SYNC_RESELLER,
    MenuItems.CREATE_SKU,
    MenuItems.CREATE_PLAN
  ];

  constructor(private router: Router,
              private communicationService: CommunicationService) {
    this.communicationService.activeMenuItem.subscribe((activeMenuItem) => this.activeMenuItem = activeMenuItem);
  }
}
