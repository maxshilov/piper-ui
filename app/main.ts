import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';
import { enableProdMode } from '@angular/core';

require('flag-icon-css/css/flag-icon.min.css');

if ('production' === process.env.NODE_ENV) {
  enableProdMode();
}
platformBrowserDynamic().bootstrapModule(AppModule);
