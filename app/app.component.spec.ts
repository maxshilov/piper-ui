import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
// Load the implementations that should be tested
import { AppComponent } from './app.component';
import { CommunicationService } from './app-communication.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { CpcActionsComponent } from './layout/cpc-actions.component';
import { CpcBreadcrumbComponent } from './layout/cpc-breadcrumb.component';
import { CpcHeaderComponent } from './layout/cpc-header.component';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { MaterialModule } from '@angular/material';

describe(`App`, () => {
  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([]), MaterialModule.forRoot()],
      declarations: [AppComponent,
        CpcActionsComponent,
        CpcBreadcrumbComponent,
        CpcHeaderComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        CookieService,
        CommunicationService,
        {provide: APP_BASE_HREF, useValue: '/'}]
    })
        .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;

    fixture.detectChanges(); // trigger initial data binding
  });

  it(`should be readly initialized`, () => {
    expect(fixture).toBeDefined();
    expect(comp).toBeDefined();
  });
});
