import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, BrowserXhr, XSRFStrategy } from '@angular/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { CSRFStrategy } from './common/csrf.strategy';
import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { CpcHeaderComponent } from './layout/cpc-header.component';
import { AuthenticationService } from './login/auth.service';
import { ResourceModule } from 'ng2-resource-rest';
import { FileSelectDirective } from 'ng2-file-upload';
import { DashboardWindow } from './scripts/windows/dashboard.window';
import { CommunicationService } from './app-communication.service';
import { CpcLeftMenuComponent } from './layout/cpc-left-menu.component';
import { CpcBreadcrumbComponent } from './layout/cpc-breadcrumb.component';
import { CpcActionsComponent } from './layout/cpc-actions.component';
import { LoginWindow } from './login/login.window';
import { PollingService } from './common/polling.service';
import { CreateSKUWindow } from './scripts/windows/create-sku.window';
import { ResellerSyncWindow } from './scripts/windows/reseller-sync.window';
import { CommonErrorHandler } from './common/common-error-handler';
import { ConfirmDialog } from './layout/cpc-confirm.dialog';
import { AuthGuard } from './login/auth-guard';
import { CPCObservableService } from './common/cpc-observable-service';
import { DialogConfirmationService } from './layout/cpc-confirm.dialog.service';
import { DataProvider } from './common/data-provider';
import { CustomBrowserXhr } from './common/custom-browser-xhr';
import { JwtHelper } from 'angular2-jwt';
import {CreatePlanWindow} from "./scripts/windows/create-plan.window";
import {WebSocketService} from "./common/websocket.service";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ResourceModule.forRoot(),
    MaterialModule.forRoot(),
    routing
  ],
  declarations: [
    FileSelectDirective,
    AppComponent,
    CpcHeaderComponent,
    CpcBreadcrumbComponent,
    CpcActionsComponent,
    CpcLeftMenuComponent,
    LoginWindow,
    DashboardWindow,
    ResellerSyncWindow,
    CreateSKUWindow,
    CreatePlanWindow,
    ConfirmDialog
  ],
  entryComponents: [
    ConfirmDialog,
  ],
  providers: [
    CookieService,
    AuthGuard,
    AuthenticationService,
    CommunicationService,
    CommonErrorHandler,
    CPCObservableService,
    DialogConfirmationService,
    WebSocketService,
    PollingService,
    DataProvider,
    JwtHelper,
    {provide: BrowserXhr, useClass: CustomBrowserXhr},
    {provide: XSRFStrategy, useClass: CSRFStrategy}
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
