import { BrowserXhr } from '@angular/http';

export class CustomBrowserXhr extends BrowserXhr {
  constructor() {
    super();
    let _build = super.build;
    this.build = () => {
      let _xhr = _build();
      _xhr.withCredentials = true;
      return _xhr;
    };
  }
}
