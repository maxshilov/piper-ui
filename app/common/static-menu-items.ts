import { CommonActionItem } from './common-action-item';

export class MenuItems {
  public static DASHBOARD: CommonActionItem = {
    label: 'Dashboard',
    routerLink: '',
    iconClass: 'apps',
    visible: true
  };

  public static SYNC_RESELLER: CommonActionItem = {
    label: 'Sync Reseller',
    routerLink: '/pba/sync-reseller',
    iconClass: 'compare_arrows',
    visible: true
  };

  public static CREATE_SKU: CommonActionItem = {
    label: 'Create SKU',
    routerLink: '/pba/create-sku',
    iconClass: 'shopping_cart',
    visible: true
  };

  public static CREATE_PLAN: CommonActionItem = {
    label: 'Create Plan',
    routerLink: '/pba/create-plan',
    visible: true,
    iconClass: 'create',
    color: 'accent'
  };

}
