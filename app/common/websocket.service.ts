import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {Observer} from "rxjs/Observer";


@Injectable()
export class WebSocketService {

  private subject: Subject<MessageEvent>;
  private ws: WebSocket;

  public connect(url: string): Subject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(url);
    }
    return this.subject;
  }

  private create(url: string): Subject<MessageEvent> {
    let ws = new WebSocket(url);
    this.ws = ws;
    // bind ws events to observable (streams)
    let observable = Observable.create(
      (obs: Observer<MessageEvent>) => {
        ws.onmessage = obs.next.bind(obs);
        ws.onerror   = obs.error.bind(obs);
        ws.onclose   = function() {
          // self.subject.unsubscribe();
          obs.complete.bind(obs);
        }

        return ws.close.bind(ws);
      });

    // on obs next (send something in the stream) send it using ws.
    let observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }else {
          console.log("ERROR: ws closed but sending data:");
          console.log(data);
        }
      }
    };

    return Subject.create(observer, observable);
  }
}
