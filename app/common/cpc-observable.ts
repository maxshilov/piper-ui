import { Observable } from 'rxjs/Observable';
import { PartialObserver } from 'rxjs/Observer';
import { Subscription } from 'rxjs/Subscription';
import { CommonErrorHandler } from './common-error-handler';

export class CPCObservable<T> extends Observable<T> {
  constructor(private _target: Observable<T>,
              private errorHandler: CommonErrorHandler) {
    super();
  }

  public subscribe(observerOrNext?: PartialObserver<T> | ((value: T) => void),
                   error?: (error: any) => void,
                   complete?: () => void): Subscription {
    error = error || this.errorHandler.handle(this.errorHandler);
    return this._target.subscribe(observerOrNext, error, complete);
  }
}
