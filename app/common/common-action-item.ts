export interface CommonActionItem {
  label?: string;
  command?: () => void;
  routerLink?: string;
  disabled?: boolean;
  iconClass?: string;
  visible?: boolean;
  color?: string;
  type?: string;
}
