import { Injectable } from '@angular/core';
import { CommunicationService } from '../app-communication.service';

@Injectable()
export class CommonErrorHandler {

  constructor(private communicationService: CommunicationService) {
  }

  public handle(self: CommonErrorHandler): ((e?: any) => void) {
    return (e?: any) => {
      console.log(e);
      if (e.status === 401) {
        self.communicationService.setLoginRequired();
      }
    };
  }
}
