import {ResellerSyncParams} from "../model/reseller-sync.params";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {CommunicationService} from "../app-communication.service";
import {CPCObservableService} from "./cpc-observable-service";
import {UserSource, ResellerSource} from "../sources";
import {AuthSource} from "../sources/auth.source";

@Injectable()
export class DataProvider {
  constructor(private communicationService: CommunicationService,
              private authSource: AuthSource,
              private reseller: ResellerSource,
              private userSource: UserSource,
              private cpcObservableService: CPCObservableService) {
  }

  public azureAuth(idToken: string, accessToken: string): Observable<string> {
    return this.observeService((self) => self.authSource.azureAuth({id_token: idToken, access_token: accessToken}));
  }

  public syncReseller(params: ResellerSyncParams): Observable<string[]> {
    return this.observeService((self) => self.reseller.sync(params));
  }

  public logout() {
    return this.observeService((self) => self.userSource.get({id: 'logout'}));
  }

  private isAuthenticated(): boolean {
    return this.communicationService.isAuthenticated();
  }

  private observeService(query: (self: DataProvider) => any): Observable<any> {
    return this.cpcObservableService.create(query(this).$observable);
  }

  private observeOrDefault(predicate: boolean, query: (self: DataProvider) => any): Observable<any> {
    return predicate ? this.observeService(query) : this.defaultScreen();
  }

  private defaultScreen(): Observable<any> {
    return Observable.from([]);
  }
}
