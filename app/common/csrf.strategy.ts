import { XSRFStrategy, Request } from '@angular/http';
import { CommunicationService } from '../app-communication.service';
import { Injectable } from '@angular/core';

@Injectable()
export class CSRFStrategy implements XSRFStrategy {
  private _csrfHeaderName: string = 'x-csrftoken';

  constructor(private communicationService: CommunicationService) {
  }

  public configureRequest(req: Request): void {
    let csrfToken = this.communicationService.getCSRFToken();
    if (csrfToken) {
      req.headers.set(this._csrfHeaderName, csrfToken);
    }
  }
}
