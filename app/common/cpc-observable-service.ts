import { Observable } from 'rxjs/Observable';
import { PartialObserver } from 'rxjs/Observer';
import { Subscription } from 'rxjs/Subscription';
import { CommonErrorHandler } from './common-error-handler';
import { Injectable } from '@angular/core';
import { CPCObservable } from './cpc-observable';

@Injectable()
export class CPCObservableService {
  constructor(private errorHandler: CommonErrorHandler) {
  }

  public create<T>(target: Observable<T>): CPCObservable<T> {
    return new CPCObservable(target, this.errorHandler);
  }
}
