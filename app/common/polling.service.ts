import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable, ObservableInput } from 'rxjs/Observable';

@Injectable()
export class PollingService {

  private _pollings: { [id: string]: Subject<boolean> } = {};

  public startPolling<T>(id: string, interval: number,
                         callback: (value: number, index: number) => ObservableInput<T>): Observable<T> {
    if (this._pollings[id]) {
      this.stopPolling(id);
    } else {
      this._pollings[id] = new Subject<boolean>();
    }
    return Observable
        .interval(interval)
        .flatMap(callback)
        .takeUntil(this._pollings[id]);
  }

  public stopPolling(id: string) {
    if (this._pollings[id]) {
      this._pollings[id].next(true);
    }
  }
}
