export interface ErrorMessageHolder {
  setErrorMessage(message: string): void;
  getErrorMessage(): string;
}
