export interface IQueryInput {
  $limit?: number;
  $offset?: number;
}
