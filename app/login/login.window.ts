import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { CommunicationService } from '../app-communication.service';
import { CommonWindow } from '../layout/common.window';
import { Router } from '@angular/router';
import { DataProvider } from '../common/data-provider';

@Component({
  templateUrl: 'login.template.html'
})

export class LoginWindow extends CommonWindow implements OnInit {

  constructor(protected communicationService: CommunicationService,
              private authService: AuthenticationService,
              private dataProvider: DataProvider,
              private router: Router) {
    super(communicationService);

    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/dashboard']);
    } else if (!!window.location.hash) {
      this.authService.handleLogin(window.location.hash);
    } else {
      this.authService.login();
    }
  }

  public ngOnInit() {
    console.log('login.window ngOnInit');
    let params = this.router.parseUrl(this.router.url);
    this.communicationService.setCSRFToken(params.queryParams['token']);
  }

  public login(event: any) {
    event.preventDefault();
    this.authService.login();
  }
}
