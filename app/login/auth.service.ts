import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Config } from '../app-config';
import { CommunicationService } from '../app-communication.service';
import { DataProvider } from '../common/data-provider';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthenticationService {
  constructor(private router: Router,
              private dataProvider: DataProvider,
              private jwtHelper: JwtHelper,
              private communicationService: CommunicationService) {
    this.communicationService.isLoginRequired.subscribe((isLoginRequired) => isLoginRequired && this.login());
  }

  public login() {
    window.location.replace(Config.getAuthorizeUrl());
  }

  public isAuthenticated(): boolean {
    return !!this.communicationService.session && !!this.communicationService.user;
  }

  public logout() {
    this.communicationService.user = null;
    this.communicationService.idToken = null;
    this.communicationService.session = null;
    this.communicationService.sessionHash = null;
    window.location.replace(Config.getLogoutUrl());
  }

  public handleLogin(fragment: string) {
    if (fragment) {
      let params = this.parseTokens(fragment);
      this.communicationService.idToken = params['id_token'];
      this.communicationService.user = this.jwtHelper.decodeToken(params['access_token']);

      this.dataProvider.azureAuth(params['id_token'], params['access_token'])
        .subscribe((res:any) => this.setSession(res), (err) => this.handleError(err));
    }
  }

  private parseTokens(fragment: string): {} {
    return fragment.substring(1).split('&').reduce((acc, kv) => {
      let pair = kv.split('=');
      acc[pair[0]] = pair[1];
      return acc;
    }, {});
  }

  private setSession(res: any) {
    this.communicationService.setCSRFToken(res['csrf_token']);
    this.communicationService.session = res['session'];
    this.communicationService.sessionHash = res['session_hash'];
    this.router.navigate(['/dashboard']);
  }

  private handleError(err: any) {
    console.error('Azure Auth error: ' + err);
    this.login();
  }
}
