import { ResourceCRUD } from 'ng2-resource-rest';
import { Config } from '../app-config';

export class BaseResourceCRUD<TQuery, TShort, TFull> extends ResourceCRUD<TQuery, TShort, TFull> {

  public getHeaders(): any {
    let headers = super.getHeaders();
    headers['X-Session-Id'] = Config.getSession();
    headers['X-Session-Hash'] = Config.getSessionHash();
    return headers;
  }
}
