import { User } from '../model/user';
import { Injector, Injectable } from '@angular/core';
import { ResourceParams, ResourceCRUD } from 'ng2-resource-rest';
import { Http } from '@angular/http';
import { Config } from '../app-config';
import { IQueryInput } from '../common/query-input';
import { BaseResourceCRUD } from './base.resource';

@Injectable()
@ResourceParams({
  url: Config.getBackendApiUrl('users'),
})
export class UserSource extends BaseResourceCRUD<IQueryInput, User, User> {
}
