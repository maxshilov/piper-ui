import {Injectable, Injector} from "@angular/core";
import {ResourceParams, ResourceAction, ResourceMethod} from "ng2-resource-rest";
import { Config } from "../app-config";
import { IQueryInput } from "../common/query-input";
import { BaseResourceCRUD } from "./base.resource";
import {Http, RequestMethod} from "@angular/http";
import {ResellerSyncParams} from "../model/reseller-sync.params";

@Injectable()
@ResourceParams({
  url: Config.getBackendApiUrl('reseller'),
})
export class ResellerSource extends BaseResourceCRUD <IQueryInput, string, string> {

  @ResourceAction({
    method: RequestMethod.Post,
    path: '/sync'
  })
  sync: ResourceMethod<ResellerSyncParams, any>;

}
