import { Injector } from '@angular/core';
import { ResourceParams, ResourceMethod, ResourceAction, } from 'ng2-resource-rest';
import { Http, RequestMethod } from '@angular/http';
import { Config } from '../app-config';
import { IQueryInput } from '../common/query-input';
import { ResourceCRUD } from 'ng2-resource-rest';

@ResourceParams({
  url: Config.getBackendApiUrl('auth'),
})
export class AuthSource extends ResourceCRUD<IQueryInput, string, string> {

  @ResourceAction({
    method: RequestMethod.Post,
    path: '/azure'
  })
  public azureAuth: ResourceMethod<{id_token: string, access_token: string}, any>;

  constructor(http: Http, injector: Injector) {
    super(http, injector);
  }
}
