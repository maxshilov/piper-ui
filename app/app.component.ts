import {Component} from "@angular/core";
import {CommunicationService} from "./app-communication.service";

@Component({
  selector: 'app',
  templateUrl: 'app.html'
})
export class AppComponent {
  constructor(public communicationService: CommunicationService) {}
}
