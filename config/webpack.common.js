const helpers = require('./helpers');
var webpack = require("webpack");

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkCheckerPlugin = require('awesome-typescript-loader').ForkCheckerPlugin;
const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
const ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');

module.exports = {
  entry: {
    "main": './app/main.ts',
    "polyfills": "./app/polyfills.browser.ts",
  },

  resolve: {
    extensions: ['', '.ts', '.js'],
    modules: [helpers.root('app'), helpers.root('node_modules')],
    alias: {
      "ng2-resource-rest": helpers.root('node_modules/ng2-resource-rest/bundles/ng2-resource-rest.umd.js')
    }
  },
  devtool: 'source-map',

  output: {
    filename: '[name].js',
    sourceMapFilename: '[name].map',
    path: helpers.root('dist')
  },
  module: {
    loaders: [
      {
        test: /\.ts$/,
        loaders: ['awesome-typescript-loader', 'angular2-template-loader', 'angular-router-loader'],
        exclude: [/node_modules/, /\.(spec|e2e)\.ts$/]
      },
      {
        test: /\.css$/,
        loaders: ["style-loader", "css-loader"]
      },
      {
        test: /\.html/,
        loader: "raw-loader",
        exclude: [helpers.root('index.html')]
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        loaders: ['file-loader?name=flag-icon-css/flags/4x3/[name].[ext]']
      }
    ]
  },

  plugins: [
    new ForkCheckerPlugin(),
    new CommonsChunkPlugin({
      name: ['polyfills', 'main'].reverse()
    }),
    new HtmlWebpackPlugin({
      chunksSortMode: 'dependency',
      title: "Product Catalog",
      inject: 'body',
      template: 'index.html'
    }),
    new ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
      /angular(\\|\/)core(\\|\/)src(\\|\/)linker/,
      helpers.root('app'), // location of your src
      {
        // your Angular Async Route paths relative to this root directory
      }
    ),
    new CopyWebpackPlugin([{from: 'app.yaml'}, {from: 'config.js'}, {from: 'static', to: "static"}]),
    new DefinePlugin({'process.env.NODE_ENV': JSON.stringify(helpers.hasProcessFlag('-p') ? 'production' : 'development')})
  ],
  devServer: {
    port: 3000,
    host: '0.0.0.0',
    historyApiFallback: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  }
}
;
