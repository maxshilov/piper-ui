var helpers = require( './helpers' );
module.exports = {

  resolve: {
    extensions: [ '', '.ts', '.js' ],
    alias: {
      "ng2-resource-rest": helpers.root('node_modules/ng2-resource-rest/bundles/ng2-resource-rest.umd.js')
    }
  },

  module: {
    postLoaders: [
      {
        test: /\.ts$/,
        loader: 'istanbul-instrumenter-loader?embedSource=true&noAutoWrap=true',
        exclude: [
          'node_modules',
          /\.(e2e|spec)\.ts$/
        ]
      }
    ],
    loaders: [
      {
        test: /\.ts$/,
        loaders: [ 'awesome-typescript-loader', 'angular2-template-loader' ]
      },
      {
        test: /\.html$/,
        loader: 'html'

      }
    ]
  }
};
